import requests

def get_json_response():
    WHAT_TO_MINE_URL = r"https://whattomine.com/coins.json?"
    WHAT_TO_MINE_URL += r"utf8=%E2%9C%93&adapt_q_280x=0&adapt_q_380=0&adapt_q_fury=0&adapt_q_470=0&adapt_q_480=3&adapt_q_570=0&adapt_q_580=0&adapt_q_vega56=0&adapt_q_vega64=0&adapt_q_750Ti=0&adapt_q_1050Ti=0&adapt_q_10606=0&adapt_q_1070=0&adapt_q_1080=0&adapt_q_1080Ti=1&adapt_1080Ti=true&eth=true"
    WHAT_TO_MINE_URL += r"&factor%5Beth_hr%5D=35.0&factor%5Beth_p%5D=140.0&factor%5Bgro_hr%5D=58.0&factor%5Bgro_p%5D=210.0&factor%5Bx11g_hr%5D=19.5&factor%5Bx11g_p%5D=170.0&factor%5Bcn_hr%5D=830.0&factor%5Bcn_p%5D=140.0&eq=true&factor%5Beq_hr%5D=685.0&factor%5Beq_p%5D=190.0&lre=true"
    WHAT_TO_MINE_URL += r"&factor%5Blrev2_hr%5D=64000.0&factor%5Blrev2_p%5D=190.0&factor%5Bns_hr%5D=1400.0&factor%5Bns_p%5D=190.0&factor%5Blbry_hr%5D=460.0&factor%5Blbry_p%5D=190.0&factor%5Bbk2b_hr%5D=2800.0&factor%5Bbk2b_p%5D=190.0&factor%5Bbk14_hr%5D=4350.0&factor%5Bbk14_p%5D=210.0&"
    WHAT_TO_MINE_URL += r"factor%5Bpas_hr%5D=1700.0&factor%5Bpas_p%5D=210.0&factor%5Bskh_hr%5D=47.5&factor%5Bskh_p%5D=190.0&factor%5Bl2z_hr%5D=420.0&factor%5Bl2z_p%5D=300.0&factor%5Bcost%5D=0.1&sort=Profitability24&volume=0"
    WHAT_TO_MINE_URL += r"&revenue=24h&factor%5Bexchanges%5D%5B%5D=&factor%5Bexchanges%5D%5B%5D=abucoins&factor%5Bexchanges%5D%5B%5D=bitfinex&factor%5Bexchanges%5D%5B%5D=bittrex&factor%5Bexchanges%5D%5B%5D=bleutrade&factor%5Bexchanges%5D%5B%5D=cryptopia"
    WHAT_TO_MINE_URL += r"&factor%5Bexchanges%5D%5B%5D=hitbtc&factor%5Bexchanges%5D%5B%5D=poloniex&factor%5Bexchanges%5D%5B%5D=yobit&dataset=Main&commit=Calculate"



    r = requests.get(url=WHAT_TO_MINE_URL)
    return r.json()

def get_ordered_coin_list():
    json = get_json_response()
    return list(json['coins'])

# Just to be able to test things
def get_ordered_coin_list_FROM_FILE():
    file = open('MockWhatToMineResponse.txt').read()
    return file.split('\n')

def find_coin_winner(candidates):
    coin_top_list = get_ordered_coin_list()
    coin_indexes = [coin_top_list.index(coin.currency_name) for coin in candidates]
    coin_indexes.sort()
    winner_coin = coin_top_list[coin_indexes[0]]
    winner_coin_list = [x for x in candidates if x.currency_name == winner_coin]
    return winner_coin_list[0]


if __name__ == "__main__":
    print(get_ordered_coin_list())
