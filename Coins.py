import psutil
import subprocess
from miners import ccminer, ethminer
import time

class Coin:
    def __init__(self, currency_name, short_name, wallet, pool, miner):
        self.currency_name = currency_name
        self.short_name = short_name
        self.wallet = wallet
        self.pool = pool
        self.miner = miner(self.pool, self.wallet)

    def __repr__(self):
        return '%s (%s)' % (self.currency_name, self.short_name)

    def start_mining(self):
        print("STARTED MINING: %s using %s" % (self, self.miner.name))
        self.miner.run_miner()

    def stop_mining(self):
        print("STOPPED MINING:", self)
        self.miner.stop_miner()
