import os
import psutil
import subprocess
import Config

def GPU_optimize():
    ## Currently not in use.
    return ["setx", "GPU_FORCE_64BIT_PTR", "0",
            "setx", "GPU_MAX_HEAP_SIZE", "100",
            "setx", "GPU_USE_SYNC_OBJECTS", "1",
            "setx", "GPU_MAX_ALLOC_PERCENT", "100",
            "setx", "GPU_SINGLE_ALLOC_PERCENT", "100"]

class Miner:
    def __init__(self, pool, wallet):
        self.pool = pool
        self.wallet = wallet

    def run_miner(self):
        print(self.cmd)
        process = psutil.Popen(self.cmd, creationflags=subprocess.CREATE_NEW_PROCESS_GROUP)
        self.pid = process.pid

    def stop_miner(self):
        try:
            process_to_kill = psutil.Process(self.pid)
            for proc in process_to_kill.children(recursive=True):
                    proc.kill()
            process_to_kill.kill()

        except psutil._exceptions.NoSuchProcess as e:
            print("No process found with pid:", self.pid)


class ccminer(Miner):
    def __init__(self, pool, wallet):
        super().__init__(pool, wallet)
        self.name = "ccMiner lyra2v2"
        self.miner_dir = Config.ROOT_FOLDER + "ccMiner\\"
        self.cmd = [self.miner_dir+"ccminer-x64-75",
               "-a", "lyra2v2",
               "-o", self.pool,
               "-u", self.wallet,
               "-i", "22",
               "-p", "x"]


class ethminer(Miner):
    def __init__(self, pool, wallet):
        super().__init__(pool, wallet)
        self.name = "Ethminer"
        self.miner_dir = Config.ROOT_FOLDER + "Ethminer\\bin\\"
        self.cmd = [self.miner_dir + "ethminer",
                    "--farm-recheck", "200",
                    "-S", self.pool,
                    "-O", self.wallet,
                    "-G"]


class nheqminer(Miner):
    def __init__(self, pool, wallet):
        super().__init__(pool, wallet)
        self.name = "nheqminer"
        self.miner_dir = Config.ROOT_FOLDER + "nheqminer\\"
        self.cmd = [self.miner_dir + "nheqminer.exe",
                    "-cd", "0",
                    "-t", "0",
                    "-l", self.pool,
                    "-u", self.wallet,
                    "-p", "x"]


class ewbfminer(Miner):
    def __init__(self, pool, wallet):
        super().__init__(pool, wallet)
        self.name = "EWBF"
        self.miner_dir = Config.ROOT_FOLDER + "ewbf\\"
        self.cmd = [self.miner_dir + "miner",
                    "--server", self.pool,
                    "--user", self.wallet,
                    "--pass", "x",
                    "--port", "3052"]
