import Config
import WhatToMineService as wtm
import time
import sys


def mine_specific_coin(short_name):
    print("Looking for...", short_name)
    coin = [coin for coin in Config.REGISTERED_COINS if coin.short_name == short_name]
    if len(coin) == 0:
        print("Cannot find currency with short name:", short_name)
    else:
        coin[0].start_mining()

if __name__ == '__main__':
    current_coin = None
    try:
        # If some coin is passed as an argument
        if(len(sys.argv) > 1):
            mine_specific_coin(sys.argv[1].upper())

        # Else let WhatToMine decide
        else:
            while True:
                suggested_coin = wtm.find_coin_winner(candidates=Config.REGISTERED_COINS)

                if (current_coin == None or suggested_coin.short_name != current_coin.short_name):
                    suggested_coin.start_mining()

                    if (current_coin != None and current_coin.miner.pid != suggested_coin.miner.pid):
                        current_coin.stop_mining()

                current_coin = suggested_coin
                time.sleep(Config.TIME_BETWEEN_CALLS)

    except KeyboardInterrupt:
        current_coin.stop_mining()
