from Coins import Coin
from miners import ccminer, ethminer, nheqminer, ewbfminer


ROOT_FOLDER = "D:\\Mining\\"

# How often WhatToMine should be called (seconds)
TIME_BETWEEN_CALLS = 60*3

# Name of currency must match response from WhatToMine
# Shortname should be the established shortname of the currency
REGISTERED_COINS= [

        Coin("Vertcoin", "VTC",
             wallet="VePTQdGoWKt1CfVaxwmKjsyXGGxo9MqEBE/60+.069",
             pool=r"stratum+tcp://lyra2.easymine.online:5000",
             miner=ccminer),

        Coin("Ethereum", "ETH",
             wallet="0x8208C63C0865713189FD53b68Ca820e8AC205878.SPOON",
             pool=r"eu1.ethermine.org:4444",
             miner=ethminer),

        Coin("Zclassic", "ZCL",
             wallet="t1NDq459afYALoKx9iVQMnMgJChwdXkkAAZ.SPOON",
             pool=r"eu.miningspeed.com",
             miner=ewbfminer),

        ]
